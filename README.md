# Xcode 7 beta 5,6 bug

There is a bug in ld binary. It works with `ld` from Xcode 7 beta 4. Copy `this repo/tools/ld` to `/Applications/Xcode-beta.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/`.