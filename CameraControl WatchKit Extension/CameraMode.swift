//
//  CameraMode.swift
//  CameraControl
//
//  Created by Pavel Pokorny on 20.08.15.
//  Copyright © 2015 Pavel Pokorny. All rights reserved.
//

import Foundation

enum CameraMode {
    case Video, Photo, Burst
}