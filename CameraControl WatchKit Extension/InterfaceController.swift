//
//  InterfaceController.swift
//  CameraControl WatchKit Extension
//
//  Created by Pavel Pokorny on 20.08.15.
//  Copyright © 2015 Pavel Pokorny. All rights reserved.
//

import WatchKit
import Foundation

enum AppState {
    case Init, Idle, Recording, Disconnected
}

class InterfaceController: WKInterfaceController {
    
    var camera: Camera
    
    var synchronized = false
        
    @IBOutlet var containerGroup: WKInterfaceGroup!
    @IBOutlet var loadingGroup: WKInterfaceGroup!
    
    @IBOutlet var modeImage: WKInterfaceImage!
    @IBOutlet var counterLabel: WKInterfaceLabel!
    @IBOutlet var recordButton: WKInterfaceButton!
    @IBOutlet var previewButton: WKInterfaceButton!
    
    var currentState = AppState.Init
    
    override init() {
        camera = CameraHero4()
        super.init()
        camera.setObserver(cameraStatusUpdate)
    }
    
    func cameraStatusUpdate() {
        
        print("camera status update")
        
        if !camera.isConnected() {
            return notifyNetworkIssues()
        }
        
        displayCameraMode(camera.currentMode())
        
        if camera.isRecording() {
            updateCounterLabel(camera.recordingTime())
            showCounterLabel(true)
            highlightRecordingButton(true)
        } else {
            showCounterLabel(false)
            highlightRecordingButton(false)
        }
        
        if !camera.isStreaming() {
            
            if camera.isLoadingPreview() {
                startLoadingAnimation()
            }
            
            if !camera.isLoadingPreview() {
                stopLoadingAnimation()
            }
            
            if camera.newPreviewAvailable() {
                setPreview(camera.getPreview())
            }
        }
        
        synchronized = true
    }
    
    override func willActivate() {
        startLoadingAnimation()
        camera.activate()
    }

    override func didDeactivate() {
        camera.deactivate()
        super.didDeactivate()
    }
    
    @IBAction func changeModeVideo() {
        if synchronized {
            synchronized = false
            camera.changeMode(CameraMode.Video)
        }
    }
    
    @IBAction func changeModePhoto() {
        if synchronized {
            synchronized = false
            camera.changeMode(CameraMode.Photo)
        }
    }
    
    @IBAction func changeModeBurst() {
        if synchronized {
            synchronized = false
            camera.changeMode(CameraMode.Burst)
        }
    }

    @IBAction func recordButtonAction() {
        if synchronized {
            synchronized = false
            camera.toggleRecording()
        }
    }
    
    @IBAction func deleteLast() {
        if synchronized {
            synchronized = false
            camera.deleteLastResource()
        }
    }
    
    @IBAction func previewButtonAction() {
    
        startLoadingAnimation()
        if camera.isStreaming() {
            camera.stopStreaming()
        } else {
            camera.startStreaming(displayStreamFrame)
        }

    }
    
    func displayStreamFrame(image: UIImage?, error: NSError?) -> Void {
        
        stopLoadingAnimation()
        
        if error != nil {
            print(error)
        } else {
            self.setPreview(image)
        }
    }
    
    // MARK: UI helpers
    
    func setPreview(data: UIImage?) {
        containerGroup.setBackgroundImage(data)
    }
    
    func startLoadingAnimation() {
        setPreview(nil)
        loadingGroup.setBackgroundImageNamed("spinner")
        loadingGroup.startAnimatingWithImagesInRange(NSMakeRange(1, 42), duration: 2, repeatCount: 0)
    }
    
    func stopLoadingAnimation() {
        loadingGroup.setBackgroundImage(nil)
    }
    
    func updateCounterLabel(text: String) {
        counterLabel.setText(text)
    }
    
    func showCounterLabel(visible: Bool) {
        counterLabel.setHidden(visible ? false : true)
    }
    
    func highlightRecordingButton(active: Bool) {
        recordButton.setBackgroundImageNamed(active ? "recording" : "not-recording")
    }
    
    func notifyNetworkIssues() {
        modeImage.setImageNamed("network-issues")
    }
    
    func displayCameraMode(mode: CameraMode?) {
        
        if mode == nil {
            return
        }
        
        var image = "network-issues"
        switch mode! {
            case .Video: image = "Video"
            case .Photo: image = "Photo"
            case .Burst: image = "Burst"
        }
        
        modeImage.setImageNamed(image)
    }


}
