//
//  CameraPrototype.swift
//  CameraControl
//
//  Created by Pavel Pokorny on 26.08.15.
//  Copyright © 2015 Pavel Pokorny. All rights reserved.
//

protocol Camera {
    func setObserver(callback: () -> Void) -> Void
    func isConnected() -> Bool
    func isRecording() -> Bool
    func currentMode() -> CameraMode?
    func recordingTime() -> String
    func isLoadingPreview() -> Bool 
    func newPreviewAvailable() -> Bool
    func getPreview() -> UIImage?
    func activate()
    func deactivate()
    func changeMode(mode: CameraMode)
    func toggleRecording()
    func deleteLastResource()
    func isStreaming() -> Bool
    func stopStreaming()
    func startStreaming(callback: (image: UIImage?, error: NSError?) -> Void)
}