//
//  CameraHero4.swift
//  CameraControl
//
//  Created by Pavel Pokorny on 20.08.15.
//  Copyright © 2015 Pavel Pokorny. All rights reserved.
//

import Foundation

enum Command: String {
    
    case GetStatus = "/camera/se"
    case ShutterStart = "/gp/gpControl/command/shutter?p=1"
    case ShutterStop = "/gp/gpControl/command/shutter?p=0"
    case GetMediaList = "/videos/DCIM/100GOPRO/"
    case SetModeVideo = "/gp/gpControl/command/mode?p=0"
    case SetModePhoto = "/gp/gpControl/command/mode?p=1"
    case SetModeBurst = "/gp/gpControl/command/mode?p=2"
    case GetVideo = "/videos/DCIM/100GOPRO/%@.MP4"
    case GetLowResVideo = "/videos/DCIM/100GOPRO/%@.LRV"
    case GetPreview = "/videos/DCIM/100GOPRO/%@.THM"
    case RestartStreaming = "/gp/gpControl/execute?p1=gpStream&c1=restart"
    case DeleteLastResource = "/gp/gpControl/command/storage/delete/last"
    
    func getUrl(param: String? = nil) -> NSURL {
        var query = self.rawValue
        if param != nil {
            query = String(format: self.rawValue, param!)
        }
        return NSURL(string: "http://" + "10.5.5.9" + query)!
    }
}

class CameraHero4 : Camera {

    private let HERO4IP = "10.5.5.9"
    
    private var session: NSURLSession
    
    private var mode: CameraMode?
    
    private var connected = false
    
    private var recording = false
    private var recordingMinutes = 0
    private var recordingSeconds = 0
    
    private var lastStatusData: NSData?
    private var statusLoopTimer: dispatch_source_t?
    
    private var freshStateAvailable: (() -> Void) = {
        print("fresh state available")
    }
    private var previewIsLoading = true
    private var lastVideoPreview: UIImage?
    
    // MARK: Streaming properties
    
    private var streaming = false
    private var lastFrameTime: NSInteger = 0
    private var player: RTSPPlayer?
    private var nextFrameTimer: dispatch_source_t?
    private var keepAliveTimer: dispatch_source_t?
    private var streamFrameAvailable:((UIImage?, NSError?) -> Void)?
    private let streamPort = 8554
    
    init() {
        session = NSURLSession.sharedSession()
    }
    
    func setObserver(stateChangedCallback: () -> Void) {
        freshStateAvailable = stateChangedCallback
    }
    
    func recordingTime() -> String {
        return String(format: "%02d:%02d", recordingMinutes, recordingSeconds)
    }
    
    func isConnected() -> Bool {
        return connected
    }
    
    func isRecording() -> Bool {
        return recording
    }
    
    func currentMode() -> CameraMode? {
        return mode
    }
    
    func isLoadingPreview() -> Bool {
        return previewIsLoading
    }
    
    func newPreviewAvailable() -> Bool {
        return lastVideoPreview != nil
    }
    
    func getPreview() -> UIImage? {
        if lastVideoPreview == nil {
            return nil
        }
        
        let copy = lastVideoPreview!.copy()
        
        lastVideoPreview = nil
        
        return (copy as? UIImage)
    }
    
    func changeMode(mode:CameraMode) {
        print("change mode")
        
        stopStatusLoop()
        var cmd: Command
        
        switch mode {
            case .Video: cmd = Command.SetModeVideo
            case .Photo: cmd = Command.SetModePhoto
            case .Burst: cmd = Command.SetModeBurst
        }
        
        sendCommand(cmd)
    }
    
    func activate() {
        print("activate")
        startStatusLoop()
        startLoadingPreview()
    }
    
    func deactivate() {
        stopStatusLoop()
        if streaming {
            stopStreaming()
        }
    }
    
    func toggleRecording() {
        stopStatusLoop()
        
        if recording {
            print("stopping recording")
            sendCommand(Command.ShutterStop)
            startLoadingPreview()
        } else {
            print("starting recording")
            sendCommand(Command.ShutterStart)
        }
    }
    
    func startStatusLoopCB(d: NSData?, r: NSURLResponse?, e: NSError?) {
        self.startStatusLoop()
    }
    
    func deleteLastResource() {
        sendCommand(Command.DeleteLastResource)
    }
    
    private func stopStatusLoop() {
        
        print("stop status loop")
        if statusLoopTimer != nil {
            dispatch_source_cancel(statusLoopTimer!)
            statusLoopTimer = nil
        }
    }
    
    private func startStatusLoop() {
        print("start status loop")

        statusLoopTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0))
        
        dispatch_source_set_timer(statusLoopTimer!, DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC, UInt64(0.25 * Double(NSEC_PER_SEC)))
        
        dispatch_source_set_event_handler(statusLoopTimer!, fetchStatus)

        dispatch_resume(statusLoopTimer!)
    }
    
    func fetchStatus() {
        
        self.session.dataTaskWithURL(Command.GetStatus.getUrl(), completionHandler: {
            (data: NSData?, response, error) -> Void in
            
            print("status received")
            
            if self.statusLoopTimer == nil {
                print("status loop was paused")
                return
            }
            
            if (error != nil || (response as! NSHTTPURLResponse).statusCode != 200) {
                self.connected = false
                return self.freshStateAvailable()
            }
            
            if (self.lastStatusData?.isEqualToData(data!) == true) {
                return
            }
            
            self.lastStatusData = NSData(data: data!)
            
            self.connected = true
            let count = data!.length / sizeof(UInt8)
            var bytes = [UInt8](count: count, repeatedValue: 0)
            data!.getBytes(&bytes, length:count * sizeof(UInt8))
            
            switch bytes[1] {
                case 0: self.mode = .Video
                case 1: self.mode = .Photo
                case 2: self.mode = .Burst
                default: self.mode = nil
            }
            

            let oldRecording = self.recording
            self.recording = bytes[29] == 1
            if oldRecording == true && self.recording == false {
                self.startLoadingPreview()
            }
            
            self.recordingMinutes = Int(bytes[13])
            self.recordingSeconds = Int(bytes[14])
            
            self.freshStateAvailable()
        }).resume()
    }
    
    private func sendCommand(cmd: Command) {
        stopStatusLoop()
        
        session.dataTaskWithURL(cmd.getUrl(), completionHandler: startStatusLoopCB).resume()
    }

// MARK: Video Preview
    
    private func startLoadingPreview() {
        print("start loading preview")
        
        previewIsLoading = true
        getLastVideoUrl() { (videoUrl, previewUrl, error) -> Void in
            
            if (error != nil) {
                self.connected = false
                NSLog("error while downloading preview %@", error!.description)
                return
            }
            
            self.session.dataTaskWithURL(previewUrl!) { (data, response, error) -> Void in

                self.previewIsLoading = false
                
                if (error != nil) {
                    print(error!.description)
                    return
                }
                
                if (response as! NSHTTPURLResponse).statusCode != 200 {
                    print("preview not available")
                    self.lastStatusData = nil
                    return
                }
                
                self.connected = true

                self.lastVideoPreview = UIImage(data: data!)
                                
                self.freshStateAvailable()
            }.resume()
        }
    }

    
    private func getLastVideoUrl(callback: (video:NSURL?, preview:NSURL?, error: NSError?) -> Void) {
        
        let url: NSURL = Command.GetMediaList.getUrl()
        
        session.dataTaskWithURL(url) { (data:NSData?, response:NSURLResponse?, error:NSError?) in
            
            if (error != nil) {
                callback(video: nil, preview: nil, error: error)
                return
            }
            do {
                let regular = "<a[^>]+href=\"(.*?)\\.THM\"[^>]*>GOPR.*?THM</a>"
                let regex = try NSRegularExpression(pattern: regular, options: [])
                
                let nsString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                let content = nsString as! String
                
                let results = regex.matchesInString(content,
                    options: [], range: NSMakeRange(0, content.characters.count))
                
                let last = (results.last?.rangeAtIndex(1))!
                let match = nsString?.substringWithRange(last)
                
                if (match == nil) {
                    let e = NSError(domain: "no preview available", code: 404, userInfo: nil)
                    callback(video: nil, preview: nil, error: e)
                    return
                }
                
                NSLog("last video %@", match!)
                
                let videoUrl = Command.GetVideo.getUrl(match)
                let previewUrl = Command.GetPreview.getUrl(match)
                
                callback(video: videoUrl, preview: previewUrl, error: nil)
                
            } catch let error {
                print("invalid regex: \(error)")
                return
            }
            }.resume()
    }
}


// MARK: Streaming extension
extension CameraHero4 {

    func isStreaming() -> Bool {
        return streaming
    }
    
    func startStreaming(callback:(image:UIImage?, error:NSError?) -> Void) {
        print("start streaming")
        streamFrameAvailable = callback
        enableStreamingOnCamera { (error) -> Void in
            print("enable streaming callback")
            if (error != nil) {
                callback(image: nil, error: error)
            } else {
                self.startStreamProcessing()
            }
        }
    }
    
    func stopStreaming() {
        print("stop streaming")
        
        if nextFrameTimer != nil {
            dispatch_source_cancel(nextFrameTimer!)
            nextFrameTimer = nil
        }
        
        if keepAliveTimer != nil {
            dispatch_source_cancel(keepAliveTimer!)
            keepAliveTimer = nil
        }
        player = nil
        streamFrameAvailable = nil
        streaming = false
        
        startLoadingPreview()
    }
    
    func getNextStreamFrame() {
        
        if self.nextFrameTimer == nil {
            print("streaming was paused")
            return
        }
        
        let startTime: NSTimeInterval = NSDate.timeIntervalSinceReferenceDate()
        if !player!.stepFrame() {
            stopStreaming()
            self.streamFrameAvailable!(nil, NSError(domain: "Stream not accessible", code: 404, userInfo: nil))
            return
        }
        
        if player != nil {
            self.streamFrameAvailable!(player!.currentImage, nil)
        }
        
        let frameTime = NSInteger(1.0 / (NSDate.timeIntervalSinceReferenceDate() - startTime))
        if lastFrameTime < 0 {
            lastFrameTime = frameTime
        } else { //LERP(frameTime, lastFrameTime, 0.8) //LERP(A,B,C) ((A)*(1.0-C)+(B)*C)
            lastFrameTime = NSInteger(((Double(frameTime))*(1.0-0.8)+(Double(lastFrameTime))*0.8))
        }
    }
    
    private func enableStreamingOnCamera(callback:(NSError?)->Void) {
        print("enable streaming on camera")
        session.dataTaskWithURL(Command.RestartStreaming.getUrl(), completionHandler: {
            (data, response, error) -> Void in
            
            print(NSString(data: data!, encoding: NSUTF8StringEncoding))
            if (error == nil) {
                print("setting up keep streaming timer")
                
                self.keepAliveTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0))
                
                dispatch_source_set_timer(self.keepAliveTimer!, DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC, UInt64(0.25 * Double(NSEC_PER_SEC)))
                
                dispatch_source_set_event_handler(self.keepAliveTimer!, self.sendKeepStreamAlive)
                
                dispatch_resume(self.keepAliveTimer!)
            }
            
            NSThread.sleepForTimeInterval(2)
            callback(nil)
        }).resume()
    }
    
    func sendKeepStreamAlive() {
        
        print("keep stream alive")
        let message = "_GPHD_:0:0:2:0.000000\n"
        
        func htons(value: CUnsignedShort) -> CUnsignedShort {
            return (value << 8) + (value >> 8);
        }
        
        let fd = socket(AF_INET, SOCK_DGRAM, 0) // DGRAM makes it UDP
        
        var addr = sockaddr_in(
            sin_len:    __uint8_t(sizeof(sockaddr_in)),
            sin_family: sa_family_t(AF_INET),
            sin_port:   htons(CUnsignedShort(streamPort)),
            sin_addr:   in_addr(s_addr: 0),
            sin_zero:   ( 0, 0, 0, 0, 0, 0, 0, 0 )
        )
        
        withUnsafeMutablePointer(&addr.sin_addr) { (ptr) -> Void in
            inet_aton(HERO4IP, ptr)
        }
        
        message.withCString { cstr -> Void in
            withUnsafePointer(&addr) { ptr -> Void in
                let addrptr = UnsafePointer<sockaddr>(ptr)
                sendto(fd, cstr, Int(strlen(cstr)), 0, addrptr, socklen_t(addr.sin_len))
            }
        }
    }
    
    private func startStreamProcessing() {
        print("start stream processing")
        
        for (var i = 0; i < 5 && player == nil; i++) {
            player = RTSPPlayer(video: "udp://:\(streamPort)", width: 432, height: 240)
        }
        
        if (player == nil) {
            print("unable to initialize player")
            return
        }
        
        player!.seekTime(0.0)
        if !player!.stepFrame() {
            print("cannot find frame")
            return
        }
        player!.setupScaler()
        
        streaming = true
        
        lastFrameTime = -1
        
        if (nextFrameTimer != nil) {
            dispatch_source_cancel(nextFrameTimer!)
        }
        
        self.nextFrameTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0))
        
        dispatch_source_set_timer(self.nextFrameTimer!, DISPATCH_TIME_NOW, UInt64((1.0 / 30) * Double(NSEC_PER_SEC)), UInt64(0.25 * Double(NSEC_PER_SEC)))
        
        dispatch_source_set_event_handler(self.nextFrameTimer!, self.getNextStreamFrame)
        
        dispatch_resume(self.nextFrameTimer!)
    }
    
}