//
//  ViewController.swift
//  CameraControl
//
//  Created by Pavel Pokorny on 20.08.15.
//  Copyright © 2015 Pavel Pokorny. All rights reserved.
//

import UIKit
import AVFoundation
//import AVKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //startVideoFramesFetching()
    }
    
    func startVideoFramesFetching()
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
            let url = NSURL(string: "http://10.5.5.9/videos/DCIM/100GOPRO/GOPR9870.MP4")
            var i: Int32 = 0
            var img = self.previewImageForLocalVideo(url!, frame: i)
            while (img != nil) {
                print("setting frame")
                img = self.previewImageForLocalVideo(url!, frame: i)
                if (img != nil) {
                    dispatch_sync(dispatch_get_main_queue(), { () -> Void in
                        self.imageView.image = img
                    })
                }
                dispatch_sync(dispatch_get_main_queue(), { () -> Void in
                    self.imageView.image = img
                })
                i++
                NSThread.sleepForTimeInterval(1)
            }
        }
    }
    
    func previewImageForLocalVideo(url:NSURL, frame:Int32) -> UIImage?
    {
        let asset = AVAsset(URL: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        
        var time = asset.duration
        let timescale = time.timescale as Int32
        let val = CMTimeValue(timescale * frame)
        time.value = val
        
        do {
            var actual = CMTime()
            let imageRef = try imageGenerator.copyCGImageAtTime(time, actualTime: &actual)
            print(actual)
            return UIImage(CGImage: imageRef)
        }
        catch let error as NSError
        {
            print("Image generation failed with error \(error)")
            return nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

